import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import {DataServiceService} from '../data-service.service';
import {Team} from '../team';
import {Game} from '../game';

@Component({
  selector: 'app-my-games',
  templateUrl: './my-games.component.html',
  styleUrls: ['./my-games.component.css']
})
export class MyGamesComponent implements OnInit {
	
    games:Game[]; 

    @Input() team: Team;

  constructor(private dataService: DataServiceService) { }


  ngOnChanges(changes: SimpleChanges) {

    if(changes['team']){
    this.getGames();
    }
  }

  ngOnInit() {
    this.getGames(); 
  }


  getGames(): void { 
    this.dataService.getGames().subscribe(temp => {
      
      var tempArr = [];

      temp.forEach(element => {
        if(element.hteamid == this.team.id || element.ateamid == this.team.id){
        tempArr.push(element);
        }
      });

     this.games = tempArr;

    }); 
  } 

}
