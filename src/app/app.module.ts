import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewLeagueTableComponent } from './view-league-table/view-league-table.component';
import { HttpClientModule} from '@angular/common/http';
import { Us1Component } from './us1/us1.component';
import { Us2Component } from './us2/us2.component';
import { Us3Component } from './us3/us3.component';
import { Us4Component } from './us4/us4.component';
import { Us5Component } from './us5/us5.component';
import { HomeComponent } from './home/home.component';
import { MyGamesComponent } from './my-games/my-games.component';
import { DataServiceService } from './data-service.service';
import { Us4childComponent } from './us4child/us4child.component';
import { Us3childComponent } from './us3child/us3child.component';
import { Us1childComponent } from './us1child/us1child.component';
import { AgmCoreModule } from '@agm/core';
import { Us2childComponent } from './us2child/us2child.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewLeagueTableComponent,
    Us1Component,
    Us2Component,
    Us3Component,
    Us4Component,
    Us5Component,
    HomeComponent,
    MyGamesComponent,
    Us4childComponent,
    Us3childComponent,
    Us1childComponent,
    Us2childComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAxXRV91FTXULmJspd_l4yU7xAu6IaPsoE',
      libraries: ['places']
    })
  ],
  providers: [DataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
