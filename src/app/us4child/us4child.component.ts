import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import {DataServiceService} from '../data-service.service';
import {Team} from '../team';
import {Game} from '../game';

@Component({
  selector: 'app-us4child',
  templateUrl: './us4child.component.html',
  styleUrls: ['./us4child.component.css']
})
export class Us4childComponent implements OnInit {

     games:Game[]; 

    @Input() team: Team;

  constructor(private dataService: DataServiceService) { }

  ngOnChanges(changes: SimpleChanges) {

    if(changes['team']){
    this.getGames();
    }
  }

  //filterobjresults = gameobj.games.filter(g=> (g.ateam === fav || g.hteam === fav ) && (g.complete === 100));


  ngOnInit() {
    this.getGames(); 
  }

 getGames(): void { 
    this.dataService.getGames().subscribe(temp => {
      
      var tempArr = [];

      temp.forEach(element => {
        if((element.hteamid == this.team.id || element.ateamid == this.team.id) && element.complete == 100){
        tempArr.push(element);
        }
      });

     this.games = tempArr;

    }); 
  } 

}
