import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Us4childComponent } from './us4child.component';

describe('Us4childComponent', () => {
  let component: Us4childComponent;
  let fixture: ComponentFixture<Us4childComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Us4childComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Us4childComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
