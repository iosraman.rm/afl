import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import {DataServiceService} from '../data-service.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Team} from '../team';
import {Game} from '../game';
import {Tip} from '../tip';

@Component({
  selector: 'app-us1child',
  templateUrl: './us1child.component.html',
  styleUrls: ['./us1child.component.css']
})
export class Us1childComponent implements OnInit {

     games:Game[]; 

      tips:Tip[];

       upcomingGameDate;

       count = 0;

       conf :number = 0;

    @Input() team: Team;

  constructor(private dataService: DataServiceService) { }

  ngOnChanges(changes: SimpleChanges) {

    if(changes['team']){
    this.getGames();
    this.getTips();
    }
  }

  ngOnInit() {
    this.getGames(); 
    this.getTips();
  }

 getGames(): void { 
    this.dataService.getGames().subscribe(temp => {
      
      var tempArr = [];

      temp.forEach(element => {
        if((element.hteamid == this.team.id || element.ateamid == this.team.id) && element.complete == 0){
        tempArr.push(element);
        }
      });
      this.upcomingGameDate = tempArr[0].date;



     this.games = tempArr;

    }); 
  } 

    getTipss(): void {

    this.dataService.getTips().subscribe(temp => {

    var tempArr2 = [];

    temp.forEach(element => {
        if(element.date == this.upcomingGameDate){
        this.conf += element.hconfidence;
        this.count++;
        //tempArr2.push(conf/(count+2));
        } 

      });

    // temp.forEach(element => {tempArr2.push(this.conf/(this.count+2));});

//      temp.forEach(element => {
  //      if(element.ateamid == this.team.id || element.ateamid == this.team.id){
    //    tempArr2.push(element);
      //  }

    //this.tips = tempArr2;
    
   // }); 

});
}       
        getTips(): void { 
       this.dataService.getTips().subscribe(temp => 
        {

        var tempArr2 = [];
        temp.forEach(element => {
        if((element.hteamid == this.team.id || element.ateamid == this.team.id))
        {
        tempArr2.push(element);
        }
this.tips = tempArr2;
this.tips.sort(this.compareFunc);


});  
  });

}


compareFunc(a, b){
    const gameAScore = a.confidence;
    const gameBScore = b.confidence;

    let compare = 0;

    if(gameAScore < gameBScore){
      compare = 1;
    }
    else if(gameAScore > gameBScore){
      compare = -1;
    }

    return compare;
  }


}