import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Us1childComponent } from './us1child.component';

describe('Us1childComponent', () => {
  let component: Us1childComponent;
  let fixture: ComponentFixture<Us1childComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Us1childComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Us1childComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
