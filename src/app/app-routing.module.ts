import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {Us1Component} from './us1/us1.component';
import {Us2Component} from './us2/us2.component';
import {Us3Component} from './us3/us3.component';
import {Us4Component} from './us4/us4.component';
import {Us5Component} from './us5/us5.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'us1', component: Us1Component },
  { path: 'us2', component: Us2Component },
  { path: 'us3', component: Us3Component },
  { path: 'us4', component: Us4Component },
  { path: 'us5', component: Us5Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
