import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Us5Component } from './us5.component';

describe('Us5Component', () => {
  let component: Us5Component;
  let fixture: ComponentFixture<Us5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Us5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Us5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
