import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Us2childComponent } from './us2child.component';

describe('Us2childComponent', () => {
  let component: Us2childComponent;
  let fixture: ComponentFixture<Us2childComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Us2childComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Us2childComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
