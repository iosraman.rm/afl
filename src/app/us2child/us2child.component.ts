import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import {DataServiceService} from '../data-service.service';
import {Team} from '../team';
import {Game} from '../game';

@Component({
  selector: 'app-us2child',
  templateUrl: './us2child.component.html',
  styleUrls: ['./us2child.component.css']
})
export class Us2childComponent implements OnInit {

     games:Game[]; 

    @Input() team: Team;

  constructor(private dataService: DataServiceService) { }

  ngOnChanges(changes: SimpleChanges) {

    if(changes['team']){
    this.getGames();
    }
  }


  ngOnInit() {
    this.getGames(); 
  }

 getGames(): void { 
    this.dataService.getGames().subscribe(temp => {
      
      var tempArr = [];
      var opponent;
      var filterobj1 = [];
      var filterobj2 = [];

      temp.forEach(element => { tempArr.push(element); });

      temp.forEach(element => {
        if((element.hteamid == this.team.id || element.ateamid == this.team.id) && element.complete == 0){
        temp.push(element);
        }
      });

      temp.forEach(element => {
        if((element.hteamid == this.team.id || element.ateamid == this.team.id) && element.complete == 100){
        temp.push(element);
        }
      });


//      for(var i=0;i<5;i++)
//      {
//      var tempArr = [];
//      if (temp[i].hteamid != this.team.id)
//      {
//      opponent = temp[i].hteam;
//      }
//      else
 //   {
  //  opponent = temp[i].ateam;
   // }
//
 //      tempArr[i].id = this.team.id;
   //    tempArr[i].ateam = opponent;
     //  tempArr[i].venue = temp[i].venue;
  //     tempArr[i].date = temp[i].date;
   //    tempArr[i].round = temp[i].round;
//} 

     this.games = tempArr;

    }); 
  } 

}
