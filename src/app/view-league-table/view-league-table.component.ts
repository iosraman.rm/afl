import { Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {DataServiceService} from '../data-service.service';
import {Team} from '../team';
import {Game} from '../game';
import {Tip} from '../tip';

@Component({
  selector: 'app-view-league-table',
  templateUrl: './view-league-table.component.html',
  styleUrls: ['./view-league-table.component.css']
})
export class ViewLeagueTableComponent implements OnInit {
	
	 teams:Team[]; 
    games:Game[]; 
    tips:Tip[]; 

  constructor(private dataService: DataServiceService) { }


  ngOnInit(): void {
  	this.getAFLTeams(); 
    this.getGames(); 
    this.getTips();
  }

  getAFLTeams(): void { 
  this.dataService.getTeams().subscribe(temp => { this.teams = temp;}); 
  } 

  getGames(): void { 
    this.dataService.getGames().subscribe(temp => {
      
      var tempArr = [];

      temp.forEach(element => {
        if(element.hteam == element.winner) tempArr.push(element);
      });

     this.games = tempArr;

    this.games.sort(this.compareFunc);

    }); 
  } 


  compareFunc(a, b){
    const gameAScore = a.hscore;
    const gameBScore = b.hscore;

    let compare = 0;

    if(gameAScore < gameBScore){
      compare = 1;
    }
    else if(gameAScore > gameBScore){
      compare = -1;
    }

    return compare;
  }

  getTips(): void { 
    this.dataService.getTips().subscribe(temp => { this.tips = temp;});  
  } 

}
