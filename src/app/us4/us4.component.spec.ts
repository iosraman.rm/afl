import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Us4Component } from './us4.component';

describe('Us4Component', () => {
  let component: Us4Component;
  let fixture: ComponentFixture<Us4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Us4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Us4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
