import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {DataServiceService} from '../data-service.service';
import {Team} from '../team';

@Component({
  selector: 'app-us4',
  templateUrl: './us4.component.html',
  styleUrls: ['./us4.component.css']
})
export class Us4Component implements OnInit {

	selectedTeam: Team;
	teams: Team[];

  constructor(private dataService: DataServiceService) { }

  ngOnInit() {
  	this.getTeams();
  }

  onSelect(team: Team):void {
  	this.selectedTeam = team;
  }

  getTeams() : void {
    this.dataService.getTeams().subscribe(temp => {this.teams = temp;})
  }

}
