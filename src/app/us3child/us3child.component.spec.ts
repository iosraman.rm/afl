import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Us3childComponent } from './us3child.component';

describe('Us3childComponent', () => {
  let component: Us3childComponent;
  let fixture: ComponentFixture<Us3childComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Us3childComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Us3childComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
